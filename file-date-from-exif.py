import os
from subprocess import Popen, PIPE
import sys
import time
from datetime import datetime
import exifread
import argparse


def read_date(directory, filename):
    fname = os.path.join(directory, filename)

    try:
        # try with exiftool:
        # exiftool -createdate file.jpg |sed 's/.*: //g'
        p1 = Popen(["exiftool", "-createdate", fname], stdout=PIPE)
        p2 = Popen(["sed", "s/.*: //g"], stdin=p1.stdout, stdout=PIPE)
        exifdate = p2.communicate()[0].strip()
        date = datetime.strptime(str(exifdate), '%Y:%m:%d %H:%M:%S')

    except ValueError:
        # try with exifread
        file = open(fname, 'rb')
        tags = exifread.process_file(file)
        # print(tags)
        if 'EXIF DateTimeOriginal' in tags.keys():
            exifdate = tags['EXIF DateTimeOriginal']
            date = datetime.strptime(str(exifdate), '%Y:%m:%d %H:%M:%S')

    if date:
        print("{}: {} ({})".format(filename, date, exifdate))
    else:
        print("{}: could not determine date".format(filename))

    return date

def change_date(directory, filename, date):
    fname = os.path.join(directory, filename)
    # apply to file, first file creation, then modification datetime
    datestr = "{}".format(date.strftime('%m/%d/%Y %H:%M:%S'))
    Popen(["SetFile", "-d", datestr, fname])
    Popen(["SetFile", "-m", datestr, fname])

def filename_filter(filename, exclude_dsc=False):
    # return filename.lower().endswith('.jpg') and filename.startswith('IMG_')
    # return filename.startswith('IMG_')
    if exclude_dsc:
        return not filename.startswith('DSC_')
    else:
        return True


###################################################
#  MAIN
###################################################
parser = argparse.ArgumentParser(description='Change file date to media creation date.')
parser.add_argument('directory',
                    help='what folder to operate on')
parser.add_argument('--do', action='store_true', default=False,
                    help="do it (default is dry run, no date change)")
parser.add_argument('-edsc', '--exclude-DSC', action='store_true',
                    dest='exclude_dsc', default=False,
                    help="for all files except those that start with DSC_*")

args = parser.parse_args()
dry = not args.do
print("analyzing folder: {}{}".format(args.directory, 
                                      " (dry run)" if dry else ""))
        
for f in os.listdir(args.directory):
    if (filename_filter(f, args.exclude_dsc)):
        try:
            date = read_date(args.directory, f)
        except:
            print("{}: ==> could not read date <==".format(f))
            continue
        if not dry:
            change_date(args.directory, f, date)
    else:
        print('skipping file: {}'.format(f))
